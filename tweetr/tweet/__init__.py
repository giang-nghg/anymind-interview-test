from flask import Blueprint

from .views import *


bp = Blueprint('tweet', __name__)


bp.add_url_rule('/hashtags/<string:hashtag>', view_func=TweetListByHashTagView.as_view('tweet_list_by_hashtag'))
bp.add_url_rule('/users/<string:screen_name>', view_func=TweetListByScreenNameView.as_view('tweet_list_by_screen_name'))
