import json

from flask import request, current_app
from flask.views import View


class TweetListView(View):
    '''
    The base class for implementing views that return a sequence of tweets.

    Subclasses must implement get_api_call() and (optionally) build_additional_api_call_params().
    '''

    methods = ['GET']
    default_limit = 30

    def dispatch_request(self, *args, **kwargs):
        api_call_params = {
            'count': int(request.args.get('limit', self.default_limit))
        }
        additional_params = self.build_additional_api_call_params(*args, **kwargs)
        api_call_params.update(additional_params)

        api_call_name = self.get_api_call()
        api_call = getattr(current_app.twitter_api_client, api_call_name)
        result = api_call(**api_call_params)
        result_json = json.dumps([x.AsDict() for x in result])

        return result_json

    def build_additional_api_call_params(self, *args, **kwargs):
        '''
        This method receives all parameters passed to dispatch_request(), and should return a key-value mapping of parameter name-value pairs to be passed to the function returned by get_api_call() when called.

        See TweetListByHashTagView and TweetListByScreenNameView for examples of how to use it.
        '''

        return {}

    def get_api_call(self):
        '''
        This method should return the name of a method of python-twitter's Api class: https://python-twitter.readthedocs.io/en/latest/twitter.html#module-twitter.api.

        See TweetListByHashTagView and TweetListByScreenNameView for examples of how to use it.
        '''

        raise NotImplementedError


class TweetListByHashTagView(TweetListView):
    def build_additional_api_call_params(self, *args, **kwargs):
        term = '#{hashtag}'.format(hashtag=kwargs['hashtag'])
        return {
            'term': term
        }

    def get_api_call(self):
        return 'GetSearch'


class TweetListByScreenNameView(TweetListView):
    def build_additional_api_call_params(self, *args, **kwargs):
        return {
            'screen_name': kwargs['screen_name']
        }

    def get_api_call(self):
        return 'GetUserTimeline'
