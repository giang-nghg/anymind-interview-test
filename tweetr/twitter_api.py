import twitter


def create_twitter_api_client(config):
    client = twitter.Api(**config)
    client.VerifyCredentials()

    return client
