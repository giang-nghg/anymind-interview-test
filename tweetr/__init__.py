import os

from flask import Flask

from tweetr.twitter_api import create_twitter_api_client
from tweetr import tweet


def create_app(test_config=None, twitter_api_client=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('tweetr.settings')

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    if twitter_api_client:
        app.twitter_api_client = twitter_api_client
    else:
        twitter_config = app.config.get_namespace('TWITTER_')
        app.twitter_api_client = create_twitter_api_client(twitter_config)

    app.register_blueprint(tweet.bp)

    return app
