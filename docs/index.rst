.. AnyMind documentation master file, created by
   sphinx-quickstart on Sun Sep 16 02:35:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AnyMind's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Module references
=================

.. automodule:: tweetr.tweet.views
   :members: TweetListView

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
