import os
import json
from json import JSONDecodeError

import pytest

from hypothesis import given, strategies as st


def test_get_tweets_by_hashtag_makes_correct_api_call(client):
    client.get('/hashtags/Python?limit=100')

    client.application.twitter_api_client.GetSearch.assert_called_once_with(term='#Python', count=100)


def test_get_tweets_by_screen_name_makes_correct_api_call(client):
    client.get('/users/Twitter?limit=100')

    client.application.twitter_api_client.GetUserTimeline.assert_called_once_with(screen_name='Twitter', count=100)


def test_get_tweets_default_limit(client):
    client.get('/hashtags/Python')
    client.application.twitter_api_client.GetSearch.assert_called_once_with(term='#Python', count=30)

    client.get('/users/Twitter')
    client.application.twitter_api_client.GetUserTimeline.assert_called_once_with(screen_name='Twitter', count=30)


@given(
    # /hashtags/<string:hashtag> accepts any string not containing a slash (http://flask.pocoo.org/docs/1.0/quickstart/#variable-rules),
    # '#' and '?' is also invalid, as discovered by a failed test generated by hypothesis.
    hashtag=st.from_regex(r'[^/#?]+', fullmatch=True),
    limit=st.integers()
)
def test_get_tweets_by_hashtag_accepted_range_of_input(client, hashtag, limit):
    response = client.get('/hashtags/{}?limit={}'.format(hashtag, limit))

    assert response.status_code == 200


@given(
    # /users/<string:screen_name> accepts any string not containing a slash (http://flask.pocoo.org/docs/1.0/quickstart/#variable-rules),
    # '#' and '?' is also invalid, as discovered by a failed test generated by hypothesis.
    screen_name=st.from_regex(r'[^/#?]+', fullmatch=True),
    limit=st.integers()
)
def test_get_tweets_by_screen_name_accepted_range_of_input(client, screen_name, limit):
    response = client.get('/users/{}?limit={}'.format(screen_name, limit))

    assert response.status_code == 200


@given(param=st.from_regex(r'[/#?]*', fullmatch=True))
def test_invalid_endpoints(client, param):
    for endpoint in [
        '/hashtags{}'.format(param),
        '/users{}'.format(param),
    ]:
        response = client.get(endpoint)
        assert response.status_code == 404


def test_get_tweets_only_accept_get_method(client):
    for endpoint in [
        '/hashtags/Python',
        '/users/Twitter'
    ]:
        response = client.get(endpoint)
        assert response.status_code == 200

        for method in ['put', 'patch', 'delete', 'post']:
            response = getattr(client, method)(endpoint)
            assert response.status_code == 405


@pytest.mark.live
@pytest.mark.parametrize('test_config', [None])
@pytest.mark.parametrize('twitter_api_client', [None])
def test_with_real_twitter_api(client):
    '''
    The purpose of this test is to alert us on Twitter API or its wrapper changes that we missed due to mocking in other tests
    '''

    endpoints = [
        '/hashtags/Python',
        '/users/Twitter',
    ]
    for endpoint in endpoints:
        response = client.get(endpoint)

        assert response.status_code == 200
        try:
            json.loads(response.data)
            # TODO: Test expected fields
        except JSONDecodeError:
            pytest.fail('{endpoint} returns invalid JSON'.format(endpoint=endpoint))
