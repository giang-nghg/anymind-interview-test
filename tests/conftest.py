import pytest

from tweetr import create_app


@pytest.fixture
def test_config():
    yield {
        'TESTING': True
    }


@pytest.fixture
def twitter_api_client(mocker):
    mock_twitter_api_client = mocker.MagicMock()
    
    # In order to return valid Werkzeug Response object
    mock_twitter_api_client.GetSearch = mocker.MagicMock(return_value='')
    mock_twitter_api_client.GetUserTimeline = mocker.MagicMock(return_value='')

    yield mock_twitter_api_client


@pytest.fixture
def app(test_config, twitter_api_client):
    yield create_app(test_config, twitter_api_client)


@pytest.fixture
def client(app):
    yield app.test_client()
