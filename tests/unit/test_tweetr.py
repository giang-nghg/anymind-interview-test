import tweetr
from tweetr import create_app


def test_instance_config_file_is_searched_for_in_non_test_setting(mocker, twitter_api_client):
    mock_app = mocker.MagicMock()
    mocker.patch('tweetr.Flask', return_value=mock_app)

    create_app(twitter_api_client=twitter_api_client)

    mock_app.config.from_pyfile.assert_called_once_with('config.py', silent=True)


def test_loading_twitter_credentials_from_config(mocker):
    test_config = {
        'TWITTER_CONSUMER_KEY': 'consumer_key',
        'TWITTER_CONSUMER_SECRET': 'consumer_secret',
        'TWITTER_ACCESS_TOKEN_KEY': 'access_token_key',
        'TWITTER_ACCESS_TOKEN_SECRET': 'access_token_secret',
    }
    mocker.patch('tweetr.create_twitter_api_client')

    create_app(test_config)

    tweetr.create_twitter_api_client.assert_called_once_with({
        'consumer_key': 'consumer_key',
        'consumer_secret': 'consumer_secret',
        'access_token_key': 'access_token_key',
        'access_token_secret': 'access_token_secret',
    })
