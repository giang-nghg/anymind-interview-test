import pytest

from tweetr.tweet.views import TweetListView


class TestTweetListView:
    def setup_method(self, method):
        self.view = TweetListView()

    def test_default_additional_api_call_params(self):
        assert self.view.build_additional_api_call_params() == {}

    def test_enforce_subclass_to_implement_get_api_call(self):
        with pytest.raises(NotImplementedError):
            self.view.get_api_call()
