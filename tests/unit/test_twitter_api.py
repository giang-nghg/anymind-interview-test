import twitter

from tweetr.twitter_api import create_twitter_api_client


def test_twitter_api_client_init_sequence(mocker):
    mock_client = mocker.MagicMock()
    mocker.patch('twitter.Api', return_value=mock_client)

    config = {
        'any': 'any',
        'second': 'second'
    }
    client = create_twitter_api_client(config)

    twitter.Api.assert_called_once_with(**config)
    assert client is mock_client
    client.VerifyCredentials.assert_called_once()
