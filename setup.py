from setuptools import setup, find_packages


setup(
	name="tweetr",
    version="0.0.1",
	packages=find_packages(),
    install_requires=[
        'Flask==1.0.2',
        'python-twitter==3.4.2'
    ]
)
